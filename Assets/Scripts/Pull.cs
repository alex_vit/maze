﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Pull : MonoBehaviour
{
    public PullObject coinPrefab;
    public int count;

    private List<PullObject> _pull;
    private Transform _transform;

    private void Awake()
    {
        _transform = transform;
    }

    void Start()
    {
        if(_pull == null)
            _pull = new List<PullObject>();
        Init();
    }

    // Update is called once per frame
    public PullObject GiveObject(string tag)
    {
        var res = _pull.FirstOrDefault(elem => elem.tag == tag);
        if (res != null)
            _pull.Remove(res);
        return res;
    }
    
    void ReturnObject(PullObject obj)
    {
        if (obj != null && !_pull.Contains(obj))
        {
            obj.transform.parent = _transform;
            _pull.Add(obj);
            obj.gameObject.SetActive(false);
        }
    }

    void Init()
    {
        for (int i = 0; i < count; i++)
        {
            var obj = Instantiate(coinPrefab, Vector3.zero, Quaternion.identity, transform);
            obj.onDeath += ReturnObject;
            ReturnObject(obj);
        }
    }
}
