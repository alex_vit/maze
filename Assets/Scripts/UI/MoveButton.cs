﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class MoveButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    // Start is called before the first frame update
    public Action OnButtonDown;
    public Action OnButtonUp;
    
    public void OnPointerDown(PointerEventData data)
    {
        OnButtonDown?.Invoke();
    }

    public void OnPointerUp(PointerEventData data)
    {
        OnButtonUp?.Invoke();
    }
}
