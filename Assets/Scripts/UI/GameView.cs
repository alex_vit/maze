﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class GameView : MonoBehaviour
{
    public Text GameEndLabel;
    public Text CoinsLabel;

    public void SetDefault()
    {
        GameEndLabel.gameObject.SetActive(false);
        UpdateCoinsCount();
    }

    public void UpdateCoinsCount()
    {
        var level = MazeApplication.Instance.levelController.current;
        CoinsLabel.text = level.Coins + "/" + level.coinsCount;
    }
    
    public void ShowEndLabel(bool isWon)
    {
        GameEndLabel.text = isWon ? "You win" : "You lose";
        GameEndLabel.gameObject.SetActive(true);
    }
}
