﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeApplication : MonoBehaviour, IController
{
    //views
    public PlayerView player;
    public MazeGrid maze;
    public EnemyView[] enemies;
    public PullObject[] coins;
    public Transform viewRoot;
    //model
    public PlayerModel playerData;
    public EnemyModel[] enemyData;
    //controllers
    public UIController applicationUi;
    public LevelController levelController;
    public MazeController mazeController = new MazeController();
    public PlayerMoveController playerMoveController = new PlayerMoveController();
    public EnemyMoveController enemyMoveController = new EnemyMoveController();
    public PathFinder pathFinder = new PathFinder();
    //config
    public CharConfig gameConfig;

    private List<IController> _controllers = new List<IController>();
   
    public static MazeApplication Instance
    {
        get => _instance;
    }

    private static MazeApplication _instance;

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else
            Destroy(gameObject);
    }

    private void Start()
    {
        Init();
    }

    void Init()
    {
        FillControllers();
        mazeController = new MazeController();
    }

    public void Notify(GameAction action, Object sender, params object[] data)
    {
        foreach (var controller in _controllers)
        {
            controller.Notify(action, sender, data);
        }
    }

    void FillControllers()
    {
        _controllers.Add(applicationUi);
        _controllers.Add(levelController);
        _controllers.Add(playerMoveController);
        _controllers.Add(enemyMoveController);
    }
}



public enum GameAction
{
    CELL_CHANGED,
    COIN_TAKED,
    PLAYER_CATCHED,
    EXIT_FOUND,
    UPDATE_INFO,
    GAME_OVER,
    GAME_START
}
