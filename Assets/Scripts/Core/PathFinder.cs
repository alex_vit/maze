﻿using System.Collections.Generic;
using UnityEngine;

public class PathFinder
{
    private List<MazeNode> FindPath(MazeNode startNode, MazeNode endNode)
    {
        var app = MazeApplication.Instance;
        List<MazeNode> openList = new List<MazeNode>();
        HashSet<MazeNode> closedList = new HashSet<MazeNode>();
        
        openList.Add(startNode);
        List<MazeNode> result = new List<MazeNode>();

        while (openList.Count > 0)
        {
            MazeNode currNode = openList[0];
            for (int i = 1; i < openList.Count; i++)
            {
                if (openList[i].FCost < currNode.FCost ||
                    openList[i].FCost == currNode.FCost && openList[i].hCost < currNode.hCost)
                {
                    currNode = openList[i];
                }
            }

            openList.Remove(currNode);
            closedList.Add(currNode);

            if (currNode == endNode)
            {                
                result = MakeFinalPath(startNode, endNode);
                break;
            }

            foreach (MazeNode node in app.mazeController.GetNeighboringNodes(currNode))
            {
                if(node.type == CellType.Wall || closedList.Contains(node))
                    continue;

                int moveCost = currNode.gCost + app.mazeController.GetManhattanDistance(currNode, node);
                if (moveCost < node.gCost || !openList.Contains(node))
                {
                    node.gCost = moveCost;
                    node.hCost = app.mazeController.GetManhattanDistance(currNode, endNode);
                    node.parent = currNode;

                    if (!openList.Contains(node))
                    {
                        openList.Add(node);
                    }
                } 
            }
        }

        return result;
    }
    
    public List<MazeNode> FindCellOnDistance(MazeNode startNode, int distance, int count)
    {
        var app = MazeApplication.Instance;
        List<MazeNode> openList = new List<MazeNode>();
        HashSet<MazeNode> closedList = new HashSet<MazeNode>();

        
        int counter = 0;
        openList.Add(startNode);
        List<MazeNode> result = new List<MazeNode>();
        
        while (result.Count < count)
        {
            int tupleIndex = 0;
            
            if(openList.Count <= 0)
                break;
            
            MazeNode currNode = openList[0];
            for (int i = 0; i < openList.Count; i++)
            {
                {
                    currNode = openList[i];
                }
            }

            openList.Remove(currNode);
            closedList.Add(currNode);

            if(counter > distance)
            {
                counter = 0;
                result.Add(currNode);
            }

            foreach (MazeNode node in app.mazeController.GetNeighboringNodes(currNode))
            {
                if (node.type == CellType.Wall || closedList.Contains(node))
                {
                    if (++tupleIndex >= app.mazeController.GetNeighboringNodes(currNode).Count)
                    {
                        tupleIndex = 0;
                        counter = 0;
                    }
                    continue;
                }
                if (!openList.Contains(node))
                {
                    openList.Add(node);
                    counter++;
                }
             
            }
        }
        
        return result;
    }

    public List<MazeNode> FindPath(GridPoint start, GridPoint target)
    {
        var app = MazeApplication.Instance;
        var startNode = app.mazeController.mazeNodes[start.xCord, start.yCord];
        var targetNode = app.mazeController.mazeNodes[target.xCord, target.yCord];
        return FindPath(startNode, targetNode);
    }

    private List<MazeNode> MakeFinalPath(MazeNode start, MazeNode final)
    {
        var res = new List<MazeNode>();
        MazeNode currNode = final;

        while (currNode != start)
        {
            res.Add(currNode);
            currNode = currNode.parent;
        }
        
        res.Reverse();
        return res;
    }

    
}
