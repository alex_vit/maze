﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class MazeGenerator
{
    public float emptyPlaceChance = .1f;
    List<Cell> cells = new List<Cell>();
    private int _rows, _columns;
    private int[,] _mazeData;
    private Stack<Cell> _cellStack;

    public int[,] Generate(int rows, int cols)
    {
        _rows = rows;
        _columns = cols;

        return CreateMaze();
    }

    public int[,] CreateMaze()
    {
        _mazeData = new int[_rows, _columns];
        _cellStack = new Stack<Cell>();
        
        for (int i = 0; i < _columns; i++)
        {
            for (int j = 0; j < _rows; j++)
            {
                if (i == 0 || j == 0 || i == _rows-1 || j == _columns-1)
                    _mazeData[i, j] = 1;
                else if ((i % 2 != 0 && j % 2 != 0) || (i == _rows/2 && j == _columns/2))
                {
                    _mazeData[i, j] = 0;
                    cells.Add(new Cell{isVisited = false, y=i, x=j});
                }
                else
                    _mazeData[i, j] = 1;
                
            }
        }
        
        CreateExit(ref _mazeData);
        
        Cell currentCell = cells.First();
     
        do
        {
            currentCell.isVisited = true;
            Cell nextCell = GetNeiborCell(currentCell);
            
            if(nextCell != null)
                RemoveWall(currentCell, nextCell);

            if (nextCell != null)
            {
                _cellStack.Push(currentCell);
                currentCell = nextCell;
            }else if (_cellStack.Count > 0)
            {
                currentCell = _cellStack.Pop();
            }

        } while (!IsCompleted());
        
        if(cells.Count != 0)
            cells.Clear();
        
        return _mazeData;
    }
    
    void CreateExit(ref int[,] data)
    {
        var numbers = new List<int>();
        for(int i=0; i< _rows; i++)
            if (i % 2 != 0)
                numbers.Add(i);

        var firstRand = numbers[Random.Range(0, numbers.Count)];
        var secondRand = Random.Range(0,2) == 0 ? 0 : _rows-1;

        if (Random.Range(0, 2) == 0)
            data[firstRand, secondRand] = -1;
        else
            data[secondRand, firstRand] = -1;
    }
    private Cell GetNeiborCell(Cell cell)
    {
        List<Cell> neighbors = new List<Cell>();

        Cell up = (cell.y - 2 > 0) ? cells.Find(c => c.x == cell.x && c.y == cell.y - 2) : null;
        Cell down = (cell.y + 2 < _columns -1) ? cells.Find(c => c.x == cell.x && c.y == cell.y + 2) : null;
        Cell left = (cell.x - 2 > 0) ? cells.Find(c => c.y == cell.y && c.x == cell.x - 2) : null;
        Cell right = (cell.x + 2 < _rows-1) ? cells.Find(c => c.y == cell.y && c.x == cell.x + 2) : null;
        
        if(up != null && !up.isVisited)
            neighbors.Add(up);
        if(down != null && !down.isVisited)
            neighbors.Add(down);
        if(left != null && !left.isVisited)
            neighbors.Add(left);
        if(right != null && !right.isVisited)
            neighbors.Add(right);

        if (neighbors.Count > 0)
        {
            int index = Random.Range(0, neighbors.Count);
            return neighbors[index];
        }

        return null;
    }

    private void RemoveWall(Cell first, Cell second)
    {
        if (first.x == second.x)
        {
            _mazeData[first.x, (second.y + first.y) / 2] = 0;
        }else if (first.y == second.y)
        {
            _mazeData[(second.x + first.x) / 2, first.y] = 0;
        }
    }

    private bool IsCompleted()
    {
        return _cellStack.Count == 0;
    }
}

public class Cell
{
    public bool isVisited;
    public int x;
    public int y;
}
