﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utils
{
    public static Vector3 GetDirection(GridPoint prevPoint, GridPoint nextPoint)
    {
        Vector2 dir = new Vector2(nextPoint.xCord, nextPoint.yCord) - new Vector2(prevPoint.xCord, prevPoint.yCord);
        dir.Normalize();
        
        if(Mathf.Abs(dir.y) > Mathf.Abs(dir.x))
            return dir.y > 0 ? Vector3.down : Vector3.up;
        if (Mathf.Abs(dir.x) > Mathf.Abs(dir.y))
            return dir.x > 0 ? Vector3.right : Vector3.left;
        return Vector3.zero;
    }
    
    public static GridPoint GetPointByDirection(GridPoint point, Vector3 direction)
    {
        GridPoint res = point;

        if (direction == Vector3.up)
            res.yCord -= 1;
        if (direction == Vector3.down)
            res.yCord += 1;
        if (direction == Vector3.left)
            res.xCord -= 1;
        if (direction == Vector3.right)
            res.xCord += 1;

        return res;
    }
}
