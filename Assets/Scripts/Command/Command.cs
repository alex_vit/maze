﻿using UnityEngine;

public abstract class Command
{
    public abstract void Execute();
}

public class MoveDown : Command
{
    public override void Execute()
    {
        MazeApplication.Instance.playerMoveController.UpdateDirection(Vector3.down);
    }
}

public class MoveLeft : Command
{
    public override void Execute()
    {
        MazeApplication.Instance.playerMoveController.UpdateDirection(Vector3.left);
    }
}

public class MoveRight : Command
{
    public override void Execute()
    {
        MazeApplication.Instance.playerMoveController.UpdateDirection(Vector3.right);
    }
}

public class MoveUp : Command
{
    public override void Execute()
    {
        MazeApplication.Instance.playerMoveController.UpdateDirection(Vector3.up);
    }
}


