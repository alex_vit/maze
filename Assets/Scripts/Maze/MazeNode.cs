﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeNode
{
    public int xGrid;
    public int yGrid;
    public Vector3 position;
    public CellType type;

    public int hCost;
    public int gCost;
    public int FCost
    {
        get { return gCost + hCost; }
    }

    public MazeNode parent;

    public MazeNode(Vector3 pos, CellType cellType, int xCoord, int yCoord)
    {
        xGrid = xCoord;
        yGrid = yCoord;
        position = pos;
        type = cellType;
    }
}

public struct GridPoint
{
    public int xCord;
    public int yCord;

    public void Fill(Vector2 data)
    {
        xCord = (int)data.x;
        yCord = (int) data.y;
    }
}

public enum CellType
{
    Ground,
    Wall
}
