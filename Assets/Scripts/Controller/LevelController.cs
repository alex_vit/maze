﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LevelController : MonoBehaviour, IController
{
    public Pull pull;
    [SerializeField]
    LevelModel[] levels;
    
    public LevelModel current => _level;
    private LevelModel _level;

    public int LevelCount
    {
        get { return levels.Length; }
    }
    private bool isPlaying;

    public void LoadLevel(int levelNum)
    {
        var app = MazeApplication.Instance;
        _level = levels[levelNum - 1];

        app.mazeController.Init();

        FillLevelParams();
        FillPlayer();
        FillCoins();
        FillEnemies();
        
        app.playerMoveController.Init();
        app.enemyMoveController.Init();
        
        GameStart();
    }

    private void TakeCoin()
    {
        var app = MazeApplication.Instance;
        _level.IncreaseCoinCounter();
        app.Notify(GameAction.UPDATE_INFO, this, null);
    }

    private void CheckWinCondition()
    {
        if(_level.Coins >= _level.coinsCount)
            GameComplete(true);
    }

    void FillLevelParams()
    {
        _level.SetDefault();
    }

    private void FillPlayer()
    {
        var app = MazeApplication.Instance;
        app.playerData = new PlayerModel();
        app.playerData.Init(app.gameConfig);
    }

    //создание монеток 
    private void FillCoins()
    {
        var app = MazeApplication.Instance;
        app.coins = new PullObject[_level.coinsCount];
        
        var center = app.mazeController.mazeNodes[app.maze.rows / 2, app.maze.columns / 2];
        var cells = app.pathFinder.FindCellOnDistance(center, _level.coinsDistance,
            _level.coinsCount);
        
        
        for (var i = 0; i < _level.coinsCount; i++)
        {            
            //инстанцируем монетки и заносим информацию
            var coinObj = pull.GiveObject("coin");
            if(coinObj == null)
                continue;
            coinObj.transform.SetParent(app.viewRoot);
            coinObj.transform.position = cells[i].position;
            coinObj.gameObject.SetActive(true);
            app.coins[i] = coinObj;      
        }
    }
    
    //инициализация врагов и их параметров
    private void FillEnemies()
    {
        var app = MazeApplication.Instance;
        app.enemyData = new EnemyModel[_level.enemyCount];

        for (int i = 0; i < app.enemyData.Length; i++)
        {
            app.enemyData[i] = new EnemyModel(_level.enemyDefaultPos[i], app.gameConfig);
        }
    }

    private void ReleaseObjects()
    {
        foreach (var obj in MazeApplication.Instance.coins)
        {
            obj.Die();
        }
    }

    private void GameComplete(bool isWon)
    {
        isPlaying = false;
        var app = MazeApplication.Instance;
        ReleaseObjects();
        app.Notify(GameAction.GAME_OVER,this,isWon);
    }

    private void GameStart()
    {
        isPlaying = true;
        var app = MazeApplication.Instance;
        app.Notify(GameAction.GAME_START,this,null);
    }

    public void Notify(GameAction actionType, Object sender, params object[] data)
    {
        if (sender is PullObject && actionType == GameAction.COIN_TAKED)
        {
            (sender as PullObject).Die();
            TakeCoin();
        }
        if (sender is EnemyView && actionType == GameAction.PLAYER_CATCHED)
        {
            if(isPlaying)
                GameComplete(false);
        }
        if(actionType == GameAction.EXIT_FOUND)
            CheckWinCondition();
    }
}
