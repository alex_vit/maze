﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI
{
    private List<MazeNode> walkingRoute;
    private int id;

    public EnemyAI(int enemyId)
    {
        id = enemyId;
        Init();
    }

    public void Init()
    {
        walkingRoute = new List<MazeNode>();
        UpdatePosition();
    }

    public void UpdatePosition()
    {
        var app = MazeApplication.Instance;    
        var point = app.enemyData[id].nextPoint;

        var nextPoint = GetNextCellNumber();
        var dir = Utils.GetDirection(app.enemyData[id].nextPoint, nextPoint);

        app.enemyData[id].prevPoint = point;
        app.enemyData[id].nextPoint = nextPoint;
        app.enemyData[id].direction = dir;
        app.enemyData[id].nextPos = app.mazeController.GetNodePosition(nextPoint);
    }

    public void UpdateFollowPosition()
    {
        var app = MazeApplication.Instance;
        var enemyData = app.enemyData[id];
        
        RecalculateRoute();
        
        if(walkingRoute == null || walkingRoute.Count == 0)
            return;
        
        enemyData.prevPoint = enemyData.nextPoint;
        enemyData.nextPoint.xCord = walkingRoute[0].xGrid;
        enemyData.nextPoint.yCord = walkingRoute[0].yGrid;
        enemyData.direction = Utils.GetDirection(enemyData.prevPoint, enemyData.nextPoint);
        enemyData.nextPos = app.mazeController.GetNodePosition(enemyData.nextPoint);
    }

    private GridPoint GetNextCellNumber()
    {
        var app = MazeApplication.Instance;
        var rowsCount = app.maze.rows;
        var colsCount = app.maze.rows;

        var nextAIPoint = app.enemyData[id].nextPoint;
        var prevAIPoint = app.enemyData[id].prevPoint;
        
        List<GridPoint> res = new List<GridPoint>();
        GridPoint tmpRes;

        int xCheck, yCheck;
        
        //up side
        xCheck = nextAIPoint.xCord;
        yCheck = nextAIPoint.yCord - 1;

        if (yCheck >= 0 && yCheck != prevAIPoint.yCord &&
            app.mazeController.mazeNodes[xCheck, yCheck].type != CellType.Wall)
        {
            tmpRes.xCord = xCheck;
            tmpRes.yCord = yCheck;
            res.Add(tmpRes);
        }
        
        //down side
        xCheck = nextAIPoint.xCord;
        yCheck = nextAIPoint.yCord + 1;

        if (yCheck < colsCount && yCheck != prevAIPoint.yCord &&
            app.mazeController.mazeNodes[xCheck, yCheck].type != CellType.Wall)
        {
            tmpRes.xCord = xCheck;
            tmpRes.yCord = yCheck;
            res.Add(tmpRes);
        }
        
        //right side
        xCheck = nextAIPoint.xCord + 1;
        yCheck = nextAIPoint.yCord;

        if (xCheck < rowsCount && xCheck != prevAIPoint.xCord &&
            app.mazeController.mazeNodes[xCheck, yCheck].type != CellType.Wall)
        {
            tmpRes.xCord = xCheck;
            tmpRes.yCord = yCheck;
            res.Add(tmpRes);
        }
        
        //left side
        xCheck = nextAIPoint.xCord - 1;
        yCheck = nextAIPoint.yCord;

        if (xCheck > 0 && xCheck != prevAIPoint.xCord &&
            app.mazeController.mazeNodes[xCheck, yCheck].type != CellType.Wall)
        {
            tmpRes.xCord = xCheck;
            tmpRes.yCord = yCheck;
            res.Add(tmpRes);
        }
        //если нашли такую клетку кроме той с которой пришли
        if (res.Count > 0)
        {
            int index = Random.Range(0, res.Count);
            return res[index];
        }
        //если не нашли делаем реверс
        return prevAIPoint;
    }

    private void RecalculateRoute()
    {
        var app = MazeApplication.Instance;
        walkingRoute = app.pathFinder.FindPath(app.enemyData[id].nextPoint, app.playerData.nextPoint);
    }
}
