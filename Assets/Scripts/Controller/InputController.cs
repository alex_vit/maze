﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputController : MonoBehaviour
{
    public MoveButton upButton;
    public MoveButton downButton;
    public MoveButton leftButton;
    public MoveButton rightButton;

    private Command Up, Down, Left, Right;
    
    private void Start()
    {
        Up = new MoveUp();
        Down = new MoveDown();
        Left = new MoveLeft();
        Right = new MoveRight();

        upButton.OnButtonDown += () => Up.Execute();
        downButton.OnButtonDown += () => Down.Execute();
        leftButton.OnButtonDown += () => Left.Execute();
        rightButton.OnButtonDown += () => Right.Execute();
    }
}