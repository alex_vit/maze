﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public delegate void EmptyAction();
    
public class UIController : MonoBehaviour, IController
{
    public GameObject menuScreen;
    public GameView gameScreen;

    private GameObject _currScreen;
    
    public Button startButton;
    public Button closeButton;
    public LevelSwitchView switchView;

    // Start is called before the first frame update
    void Start()
    {
        startButton.onClick.AddListener(StartGame);
        closeButton.onClick.AddListener(Close);
        
        SwitchScreen(menuScreen);
    }

    void StartGame()
    {
        //start game with the level number
        SwitchScreen(gameScreen.gameObject);
        MazeApplication.Instance.levelController.LoadLevel(switchView.LevelNum);
    }

    void Close()
    {
        Application.Quit();
    }

    void SwitchScreen(GameObject screen)
    {
        if(_currScreen != null)
            _currScreen.SetActive(false);
        _currScreen = screen;
        _currScreen.SetActive(true);
    }

    public void Notify(GameAction actionType, Object sender, params object[] data)
    {
        if (sender is LevelController && actionType == GameAction.UPDATE_INFO)
            gameScreen.UpdateCoinsCount();
        if(sender is LevelController && actionType == GameAction.GAME_OVER)
            StartCoroutine(ShowEndLabel((bool)data[0]));
        if(sender is LevelController && actionType == GameAction.GAME_START)
            gameScreen.SetDefault();
    }

    IEnumerator ShowEndLabel(bool isWon)
    {
        gameScreen.ShowEndLabel(isWon);
        yield return new WaitForSeconds(2f);
        SwitchScreen(menuScreen);
    }
}
