﻿using UnityEngine;

public class PlayerMoveController : IController
{
    public bool IsMoving;

    private Vector3 _currentDirection;
    public void Init()
    {
        //устанавливаем игрока в изначальную позицию
        var app = MazeApplication.Instance;
        app.playerData.defaultPoint.Fill(app.levelController.current.playerDefaultPos);
        var defaultPos = app.mazeController.GetNodePosition(app.playerData.defaultPoint);
        
        app.playerData.prevPoint = app.playerData.defaultPoint;
        app.playerData.nextPoint = app.playerData.defaultPoint;
        app.playerData.nextPos = defaultPos;
        app.playerData.direction = Vector3.zero;
        app.player.Init(defaultPos);

        _currentDirection = Vector3.zero;
        
        Run();
    }

    private void Run()
    {
        var app = MazeApplication.Instance;
        app.player.Run();
    }

    private void Stop()
    {
        var app = MazeApplication.Instance;
        app.player.Stop();
        IsMoving = false;
    }

    private void HandleCellChange()
    {
        var app = MazeApplication.Instance;
        GridPoint nextPoint = Utils.GetPointByDirection(app.playerData.nextPoint, _currentDirection);
        var isFreeCell = app.mazeController.IsFreeNode(nextPoint);
        
        CheckExit();
        
        //если это не стена, обновляем данные модели и заставляем двигаться туда view
        if (isFreeCell)
        {
            app.playerData.prevPoint = app.playerData.nextPoint;
            app.playerData.nextPoint = nextPoint;
            app.playerData.nextPos = app.mazeController.GetNodePosition(nextPoint);
            app.playerData.direction = _currentDirection;
            app.player.UpdatePosition(app.playerData.nextPos);
            IsMoving = true;
        }
        else
        {
            app.playerData.prevPoint = app.playerData.nextPoint;
            IsMoving = false;
        }
    }

    private void CheckExit()
    {
        var app = MazeApplication.Instance;
        if (app.mazeController.mazeData[app.playerData.nextPoint.xCord, app.playerData.nextPoint.yCord] == -1)
        {
            app.Notify(GameAction.EXIT_FOUND, app.player, null);
        }
    }

    public void UpdateDirection(Vector3 direction)
    {
        //если направление изменилось и в том направлении нет стены меняем его
        if (_currentDirection != direction && CheckNextPosition(direction))
        {
            _currentDirection = direction;
            if(!IsMoving)
                HandleCellChange();
        }
    }

    bool CheckNextPosition(Vector3 dir)
    {
        var app = MazeApplication.Instance;
        GridPoint nextPoint = Utils.GetPointByDirection(app.playerData.nextPoint, dir);

        if (app.mazeController.IsFreeNode(nextPoint))
            return true;
        return false;
    }

    public void Notify(GameAction actionType, Object sender, params object[] data)
    {
        if (sender is PlayerView && actionType == GameAction.CELL_CHANGED)
        {
            HandleCellChange();
        }
        if(sender is LevelController && actionType == GameAction.GAME_START)
            Init();
        if(sender is LevelController && actionType == GameAction.GAME_OVER)
            Stop();
    }
}
