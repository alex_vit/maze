﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMoveController : IController
{
    private EnemyAI[] enemyAi;

    private bool isActive;
    //init enemies, defalt pos and values
    public void Init()
    {
        var app = MazeApplication.Instance;
        enemyAi = new EnemyAI[app.levelController.current.enemyCount];

        isActive = true;
        
        for (int i = 0; i < app.levelController.current.enemyCount; i++)
        {
            //app.enemyData[i] = new EnemyModel(app.levelController.current.enemyDefaultPos[i]);           
            var defaultPos = app.mazeController.GetNodePosition(app.enemyData[i].defaultPoint);
            app.enemyData[i].nextPos = defaultPos;
            app.enemyData[i].state = EnemyState.IDLE;
            
            app.enemies[i].Init(app.enemyData[i].nextPos);
            app.enemies[i].ApplyState(app.enemyData[i].state);
            //создаем бота
            enemyAi[i] = new EnemyAI(i);
            enemyAi[i].Init();
        }
    }

    private void Run()
    {
        foreach (var enemy in MazeApplication.Instance.enemies)
            enemy.Run();
    }

    private void Stop()
    {
        isActive = false;
        foreach (var enemy in MazeApplication.Instance.enemies)
            enemy.Stop();
    }

    private void HandleCellChange(object sender)
    {
        if(!isActive)
            return;
        var app = MazeApplication.Instance;
        var enemy = sender as EnemyView;
        var enemyData = app.enemyData[enemy.id];
        
        CheckState();
        //если состояние бота IDLE
        if (enemyData.state == EnemyState.IDLE)
        {
            //находим ближайшую свободную клетку и обновляем модель
            enemyAi[enemy.id].UpdatePosition();
        }
        if (enemyData.state == EnemyState.WALKING)
        {
            //обновляем маршрут
            enemyAi[enemy.id].UpdateFollowPosition();
        }
    }

    public void CheckState()
    {
        {
            var app = MazeApplication.Instance;
            var playerPos = app.playerData.nextPos;
            
            var awakenDist = app.levelController.current.awakenDistance;
            var lostDist = app.levelController.current.lostDistance;
        
            for (int i = 0; i < app.enemyData.Length; i++)
            {
                var enemyPos = app.enemyData[i].nextPos;
                var xDist = Mathf.Abs(playerPos.x - enemyPos.x) / app.maze.stepSize;
                var yDist = Mathf.Abs(playerPos.y - enemyPos.y) / app.maze.stepSize;

                if (app.enemyData[i].state == EnemyState.WALKING && (xDist >= lostDist || yDist >= lostDist))
                    app.enemyData[i].state = EnemyState.IDLE;
                if (app.enemyData[i].state == EnemyState.IDLE && (xDist < awakenDist && yDist < awakenDist))
                    app.enemyData[i].state = EnemyState.WALKING;
            
                //update view
                app.enemies[i].ApplyState(app.enemyData[i].state);
            }
        }    
    }

    public void Notify(GameAction actionType, Object sender, params object[] data)
    {
        if(sender is LevelController && actionType == GameAction.GAME_START)
            Run();
        if (actionType == GameAction.PLAYER_CATCHED || actionType == GameAction.GAME_OVER)
            Stop();
        if (sender is EnemyView && actionType == GameAction.CELL_CHANGED)
            HandleCellChange(sender);
    }
    
    
}
