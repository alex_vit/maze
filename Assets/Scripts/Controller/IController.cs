﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IController
{
    void Notify(GameAction actionType, Object sender, params object[] data);
}
