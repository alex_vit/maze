﻿using System.Collections.Generic;
using UnityEngine;

public class MazeController
{
    public int[,] mazeData { get; private set; }
    public MazeNode[,] mazeNodes { get; private set; }

    private MazeGenerator _mazeGenerator;
    

    public MazeController()
    {
        _mazeGenerator = new MazeGenerator();
    }

    public void Init()
    {
        var app = MazeApplication.Instance;
        mazeData = _mazeGenerator.Generate(app.maze.rows, app.maze.columns);
        app.maze.DrawMaze(mazeData);
        CheckDimensions();
    }

    void CheckDimensions()
    {
        int oldDimX = 0, oldDimY = 0;
        
        var xDim = mazeData.GetUpperBound(0) + 1;
        var yDim = mazeData.GetUpperBound(1) + 1;

        if (mazeNodes != null)
        {
            oldDimX = mazeNodes.GetUpperBound(0) + 1;
            oldDimY = mazeNodes.GetUpperBound(1) + 1;
        }

        var isNonReusable = mazeNodes == null || xDim != oldDimX || yDim != oldDimY;
        if(isNonReusable)
            mazeNodes = new MazeNode[xDim, yDim];

        FillNodeData(isNonReusable);
        
    }

    void FillNodeData(bool needInit)
    {
        var app = MazeApplication.Instance;
        for (int y = 0; y <= mazeNodes.GetUpperBound(1); y++)
        {
            for (int x = 0; x <= mazeNodes.GetUpperBound(0); x++)
            {
                if (needInit)
                {
                    var offset = new Vector3(app.maze.stepSize * x, -app.maze.stepSize * y, 0f);
                    var newPos = app.maze.startPoint + offset;
                    mazeNodes[x, y] = new MazeNode(newPos, (CellType) mazeData[x, y], x, y);
                }
                else
                    mazeNodes[x, y].type = (CellType) mazeData[x, y];
            }
        }
    } 

    public Vector3 GetNodePosition(GridPoint point)
    {
        return mazeNodes[point.xCord, point.yCord].position;
    }

    public bool IsFreeNode(GridPoint point)
    {
        if (point.xCord < 0 || point.xCord > mazeData.GetUpperBound(0))
            return false;
        if (point.yCord < 0 || point.yCord > mazeData.GetUpperBound(1))
            return false;
        return mazeNodes[point.xCord, point.yCord].type != CellType.Wall;
    }


    public List<MazeNode> GetNeighboringNodes(MazeNode node)
    {
        var app = MazeApplication.Instance;
        List<MazeNode> result = new List<MazeNode>();

        int xCheck, yCheck;
        
        //up side
        xCheck = node.xGrid;
        yCheck = node.yGrid - 1;

        if (yCheck >= 0 && yCheck < app.maze.columns)
        {
            result.Add(mazeNodes[xCheck,yCheck]);
        }
        //down side
        xCheck = node.xGrid;
        yCheck = node.yGrid + 1;

        if (yCheck >= 0 && yCheck < app.maze.columns)
        {
            result.Add(mazeNodes[xCheck, yCheck]);
        }
        //left side
        xCheck = node.xGrid - 1;
        yCheck = node.yGrid;

        if (xCheck >= 0 && xCheck < app.maze.rows)
        {
            result.Add(mazeNodes[xCheck,yCheck]);
        }
        //right side
        xCheck = node.xGrid + 1;
        yCheck = node.yGrid;

        if (xCheck >= 0 && xCheck < app.maze.rows)
        {
            result.Add(mazeNodes[xCheck,yCheck]);
        }

        return result;
    }

    public int GetManhattanDistance(MazeNode firstNode, MazeNode secondNode)
    {
        int dx = Mathf.Abs(firstNode.xGrid - secondNode.xGrid);
        int dy = Mathf.Abs(firstNode.yGrid - secondNode.yGrid);
        
        return dx + dy;
    }
    
}


