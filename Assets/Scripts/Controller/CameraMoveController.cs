﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMoveController : MonoBehaviour
{
    public Camera targetCamera;
    public Transform target;

    private Transform _transform;
    
    void Awake()
    {
        _transform = targetCamera.transform;
    }

    // Update is called once per frame
    void Update()
    {
        TrackPositon();
    }

    void TrackPositon()
    {
        var newPos = new Vector3(target.position.x, target.position.y, 0f);
        var offset = new Vector3(newPos.x - _transform.position.x, newPos.y - _transform.position.y , 0);
        
        var offsetX = CheckBoundsX(offset) ? offset.x : 0;
        var offsetY = CheckBoundsY(offset) ? offset.y : 0;
        
        targetCamera.transform.position += new Vector3(offsetX, offsetY, 0f);      
    }

    bool CheckBoundsX(Vector3 offset)
    {
        var bounds = targetCamera.OrthographicBounds();
        if ((bounds.min.x + offset.x) >= -5f && (bounds.max.x + offset.x <= 5f)) 
            return true;
        return false;
    }
    
    bool CheckBoundsY(Vector3 offset)
    {
        var bounds = targetCamera.OrthographicBounds();
        if((bounds.min.y + offset.y) >= -5f && (bounds.max.y + offset.y <= 5f))
            return true;
        return false;
    }
}