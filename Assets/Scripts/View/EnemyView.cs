﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyView : MonoBehaviour
{
    public int id;
    private Transform _transform;
    private SpriteRenderer image;
    void Awake()
    {
        _transform = transform;
        image = GetComponent<SpriteRenderer>();
    }

    public void Init(Vector3 pos)
    {
        gameObject.SetActive(true);
        //устанавливаем изначальну позицию
        _transform.position = pos;
    }

    public void Run()
    {
        StartCoroutine(MoveProcess());
    }

    public void Stop()
    {
        gameObject.SetActive(false);
        StopAllCoroutines();
    }

    public void ApplyState(EnemyState enemyState)
    {
        if(enemyState == EnemyState.IDLE)
            image.color = Color.yellow;
        if(enemyState == EnemyState.WALKING)
            image.color = Color.red;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            MazeApplication.Instance.Notify(GameAction.PLAYER_CATCHED, this);
        }
    }

    IEnumerator MoveProcess()
    {
        var app = MazeApplication.Instance;
        var stepSize = app.maze.stepSize;
        
        while (true)
        {
            //получаем откуда то направление движения
            var dir = app.enemyData[id].direction; 
            
            if (dir != Vector3.zero)
            {
                //следующая позиция 
                var pos = app.enemyData[id].nextPos;
                if (Vector3.Magnitude(pos - _transform.position) <= 0.1f)
                {
                    //переместились на следующую клетку
                    app.Notify(GameAction.CELL_CHANGED, this, null);
                }
                _transform.localPosition += (dir * app.enemyData[id].speed * (stepSize / 1000f));
            }
            yield return new WaitForFixedUpdate();       
        }
    }
}
