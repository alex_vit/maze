﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSwitchView : MonoBehaviour
{
    public Text levelNumberText;
    public Button prevLevelButton;
    public Button nextLevelButton;

    public int LevelNum { get; private set; }
    private int _maxLevelsCount;
    void Start()
    {
        LevelNum = 1;
        prevLevelButton.onClick.AddListener(DecreaseLevel);
        nextLevelButton.onClick.AddListener(IncreaseLevel);
        _maxLevelsCount = MazeApplication.Instance.levelController.LevelCount;
        
        UpdateInfo();
    }

    void IncreaseLevel()
    {
        LevelNum++;
        UpdateInfo();
    }

    void DecreaseLevel()
    {
        LevelNum--;
        UpdateInfo();
    }

    void UpdateInfo()
    {
        prevLevelButton.gameObject.SetActive(LevelNum > 1);
        nextLevelButton.gameObject.SetActive(LevelNum < _maxLevelsCount);
        levelNumberText.text = LevelNum.ToString();
    }
}
