﻿using UnityEngine;
public class MazeGrid : MonoBehaviour
{
    public float stepSize;
    public bool showDebugMode;
    public int rows, columns = 0;
    public Sprite ground, wall;
    public GameObject tilePrefab;
    public Vector3 startPoint;  
    public TileView[] mazeTileViews;
    // Start is called before the first frame update
    void Awake()
    {
        mazeTileViews = GetComponentsInChildren<TileView>();
    }

    public void DrawMaze(int[,] data)
    {
        if(data == null)
            return;

        var rMax = data.GetUpperBound(0) + 1;
        var cMax = data.GetUpperBound(1) + 1;
        
        Debug.Log(rMax + " " + cMax);

        for (int i = 0; i < rMax; i++)
        {
            for (int j = 0; j < cMax; j++)
            {
                mazeTileViews[j*rMax+i].SetCellImage(data[i,j] == 1 ? wall : ground);
            }
        }
    }
    
    public void DrawField()
    {
        var size = stepSize;
        for (int i = 0; i < columns; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                var offset = new Vector3(size * (float)j,-size * (float)i, 0);
                Instantiate(tilePrefab, startPoint + offset, Quaternion.identity, transform);
            }
        }
    }

    public void ClearField()
    {
        var children = transform.GetComponentsInChildren<TileView>();
        for (int i = 0; i < children.Length; i++)
        {
            DestroyImmediate(children[i].gameObject);
        }
    }

    void OnGUI()
    {
        if(!showDebugMode)
            return;

        int[,] maze = MazeApplication.Instance.mazeController.mazeData;

        var rMax = maze.GetUpperBound(0);
        var cMax = maze.GetUpperBound(1);
        string msg = "";

        for (int i = rMax; i >= 0; i--)
        {
            for (int j = 0; j <= cMax; j++)
            {
                if (maze[i, j] == 0)
                {
                    msg += "....";
                }
                else
                {
                    msg += "==";
                }
            }
            msg += "\n";
        }

        GUI.Label(new Rect(20, 20, 500, 500), msg);
    }
}
