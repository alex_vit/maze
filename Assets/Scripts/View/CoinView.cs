﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinView : MonoBehaviour
{
    private PullObject _pullObject;

    private void Awake()
    {
        _pullObject = GetComponent<PullObject>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            MazeApplication.Instance.Notify(GameAction.COIN_TAKED, _pullObject);
        }
    }
}
