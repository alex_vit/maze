﻿using System.Collections;
using UnityEngine;

public class PlayerView : MonoBehaviour
{
    private Transform _selfTransform;
    private Vector3 _nextPos;

    void Awake()
    {
        _selfTransform = transform;
    }

    public void Init(Vector3 pos)
    {
        _selfTransform.position = pos;
        
    }

    public void Run()
    {
        StartCoroutine(MoveProcess());
    }

    public void Stop()
    {
        StopAllCoroutines();
    }

    public void UpdatePosition(Vector3 pos)
    {
        _nextPos = pos;
    }

    IEnumerator MoveProcess()
    {
        var app = MazeApplication.Instance;
        var speed = app.playerData.speed;
        var stepSize = app.maze.stepSize;
        while (true)
        {
            var dir = app.playerData.direction;
            
            if (app.playerMoveController.IsMoving && dir != Vector3.zero)
            {
                if (Vector3.Magnitude(_nextPos - _selfTransform.position) <= 0.1f)
                {
                    //переместились на следующую клетку
                    app.Notify(GameAction.CELL_CHANGED, this, null);
                }
                _selfTransform.localPosition += dir * stepSize * speed / 1000f;
            }
            yield return new WaitForFixedUpdate();
        }
    }
}
