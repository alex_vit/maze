﻿using System;
using UnityEngine;

public class PullObject : MonoBehaviour
{
    public Action<PullObject> onDeath;
    public string tag;

    public void Die()
    {
        if (onDeath != null)
            onDeath(this);
    }
}
