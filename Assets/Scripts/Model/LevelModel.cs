﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level")]
public class LevelModel : ScriptableObject
{
    public int coinsCount;
    public int awakenDistance;
    public int lostDistance;
    public int coinsDistance;
    public Vector3 exitPoint;
    public Vector2 playerDefaultPos;
    public int enemyCount;
    public Vector2[] enemyDefaultPos;

    public int Coins => _coinsToWin;
    private int _coinsToWin = 0;

    public void IncreaseCoinCounter()
    {
        _coinsToWin++;
    }

    public void SetDefault()
    {
        _coinsToWin = 0;
    }

}
