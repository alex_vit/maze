﻿using UnityEngine;

public class PlayerModel
{
    public GridPoint prevPoint;
    public GridPoint nextPoint;
    public float speed = 5f;
    public GridPoint defaultPoint;
    public Vector3 nextPos = Vector3.zero;
    public Vector3 direction = Vector3.zero;

    public void Init(CharConfig config)
    {
        speed = config.playerSpeed;
    }
}
