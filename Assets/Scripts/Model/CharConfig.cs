﻿using UnityEngine;

[CreateAssetMenu(fileName = "Char config")]
public class CharConfig : ScriptableObject
{
    public float playerSpeed;
    public float enemySpeed;
    public float enemyFastSpeed;
}
