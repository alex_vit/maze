﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyModel
{
    public int id; //идентификатор для определния 
    public GridPoint prevPoint;
    public GridPoint nextPoint;
    
    public GridPoint defaultPoint;
    public Vector3 nextPos = Vector3.zero;
    public Vector3 direction = Vector3.zero;
    public EnemyState state;
    public float speed {
        get
        {
            if (state == EnemyState.WALKING) return _followSpeed;
            return _idleSpeed;
        }
    }

    float _idleSpeed;
    float _followSpeed;

    public EnemyModel(Vector2 point, CharConfig config)
    {
        defaultPoint.Fill(point);
        prevPoint = defaultPoint;
        nextPoint = defaultPoint;

        state = EnemyState.IDLE;
        
        _idleSpeed = config.enemySpeed;
        _followSpeed = config.enemyFastSpeed;
    }
}

public enum EnemyState
{
    IDLE, 
    WALKING
}
