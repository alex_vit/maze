﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MazeGrid))]
public class MazeEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        MazeGrid maze = (MazeGrid)target;
        if(GUILayout.Button("DrawField"))
        {
            maze.DrawField();
        }

        if (GUILayout.Button("ClearField"))
        {
            maze.ClearField();
        }
    }
}
